<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\StockAdjustment;
use App\Models\Warehouse;
use App\Models\Stock;
use App\Models\Bincard;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class StockAdjustmentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Stock Adjustment');
            $content->description(trans('admin.list'));
            $content->body($this->grid()->render());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Stock Adjustment');
            $content->description(trans('admin.edit'));
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
         return Admin::content(function (Content $content) {
            $content->header('Stock Adjustment');
            $content->description(trans('admin.create'));
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(StockAdjustment::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('warehouse.name');
            $grid->column('product.product_name');
            $grid->type('Type');
            $grid->qty('Quantity');

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(StockAdjustment::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->select('warehouse_id', 'Warehouse')->rules('required')->options(Warehouse::all()->pluck('name', 'id'));
            $form->select('product_id', 'Product')->rules('required')->options(Product::all()->pluck('product_name', 'id'));

            $activeArray = ['INVOICE'=>'INVOICE','RETURN'=>'RETURN'];
            $form->select('type', 'Type')->options($activeArray)->default('INVOICE');

            $form->number('qty')->rules('required|');
            $form->text('description', 'Description');

            $activeArray = ['ACTIVE'=>'ACTIVE','INACTIVE'=>'INACTIVE'];
            $form->select('status', 'Status')->options($activeArray)->default('ACTIVE');

            $form->display('created_at', trans('admin.created_at'));
            $form->display('updated_at', trans('admin.updated_at'));

            $form->saving(function (Form $form) {
                $productExists = Stock::where(array('product_id' => $_POST['product_id'], 'warehouse_id' => $_POST['warehouse_id']))->first();
                if($productExists && $productExists['mutual_balance'] < $_POST['qty']) {
                    return back()->with(compact('Requested transfer quantity not available.'));
                }else{
                    if($_POST['type']  == 'INVOICE') {
                        $mutualStock = $productExists['mutual_balance'] - $_POST['qty'];
                        $actualStock = $productExists['actual_balance'] - $_POST['qty'];
                    }elseif($_POST['type']  == 'RETURN'){
                        $mutualStock = $productExists['mutual_balance'] + $_POST['qty'];
                        $actualStock = $productExists['actual_balance'] + $_POST['qty'];
                    }

                    $stock = new Stock();

                    $stock->where('status', 'ACTIVE')
                        ->where(array('product_id' => $_POST['product_id'], 'warehouse_id' => $_POST['warehouse_id']))
                        ->update(['mutual_balance' => $mutualStock, 'actual_balance' => $actualStock]);


                    $product = new Product();
                    $productQty = Product::where(array('id' => $_POST['product_id']))->first();
                    if($_POST['type']  == 'INVOICE') {
                        $stockInHand = $productQty['stock_on_hand'] - $_POST['qty'];
                    }elseif($_POST['type']  == 'RETURN'){
                        $stockInHand = $productQty['stock_on_hand'] + $_POST['qty'];
                    }


                    $product->where('status', 'ACTIVE')
                            ->where(array('id' => $_POST['product_id']))
                            ->update(['stock_on_hand' => $stockInHand]);


                    //add to bin card
                    $bincard = new Bincard();
                    $bincard->product_id = $_POST['product_id'];
                    $bincard->warehouse_id = $_POST['warehouse_id'];
                    $bincard->transaction_description = $_POST['description'];
                    if($_POST['type']  == 'INVOICE') {
                        $bincard->credit = $_POST['qty'];
                        $bincard->debit = 0;
                    }elseif($_POST['type']  == 'RETURN'){
                        $bincard->credit = 0;
                        $bincard->debit = $_POST['qty'];
                    }
                    $bincard->status = 'ACTIVE';

                    $bincard->save();
                }


            });
        });
    }
}
